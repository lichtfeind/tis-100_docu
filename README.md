# tis-100_docu

This is a link collection on TIS-100 implementaions and ideas for the same.

# Docu
 * https://kk4ead.github.io/tis-100/
 * https://alandesmet.github.io/TIS-100-Hackers-Guide/
 * 
# Implementatios

## Verilog
 * https://github.com/Cognoscan/VerilogTIS100

## VHDL
 * https://github.com/jdryg/tis100cpu

## Javascript
 * https://github.com/devsnek/tis100.js

 
## Python
 * https://github.com/benWindsorCode/assemblyRunner

## C
 * https://github.com/eviltrout/tis-100

## RUST
 * https://github.com/rcolinray/tis-100-rs

# Ideas
 * https://hackaday.io/project/11227-tis-100-geiger https://hackaday.io/project/11227/logs
 * https://www.gog.com/forum/tis100/building_a_tis100
 

# Other reads
 * https://de.wikipedia.org/wiki/Transputer
 * https://de.wikipedia.org/wiki/Parallax_Propeller
 * https://en.m.wikipedia.org/wiki/Systolic_array
